## Integration tests suite

Ideally, each repository should have a unit tests suite which covers isolated functionality of functions, as much as it is possible. However, quite a lot of functionality of the platform is revealed only when a different containers communicate between other. In order to test this, we need to have a separate integration tests suite which can reflect the whole pipeline needed to build, spawn containers and initiate commands from inside of them that would test such functionality.

Thus integration tests are the ultimate test for the functionality and eventually will pave a way for full system tests.
