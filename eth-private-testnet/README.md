# Test

## Build private ethereum blockchain
``` 
docker pull ethereum/client-go

```

## Start private ethereum blockchain

``` 
export stats_address

bash start-eth.sh
```

## Build dashboard
``` 
docker-compose build

```
## Start dashboard
``` 
docker-compose up

```