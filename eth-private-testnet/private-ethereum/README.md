# Build

``` 
docker build -t private-eth .
```

## Run container
``` 
docker run --name eth -p 30301:30301 -p 30303:30303 -p 8545:8545 -v DataDir-path:/private-eth/DataDir -v ./private-eth:/private-eth/private-eth -it private-eth bash
```

## Start private ethereum blockchain

``` 
geth --identity "node033"  --rpc --rpcaddr "0.0.0.0"  --rpcport "8545" --rpccorsdomain "*" --ethstats node01:s3cr3t@http://135.181.222.170:3000 --datadir "DataDir-path" --port "30303" --allow-insecure-unlock --nodiscover --rpcapi "db,eth,net,web3,personal,miner,admin"   --networkid 1900 --nat "any"
```