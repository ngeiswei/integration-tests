#!/bin/bash

nunet-adapter() {
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
    docker pull registry.gitlab.com/nunet/nunet-adapter:temp
    docker tag registry.gitlab.com/nunet/nunet-adapter:temp registry.gitlab.com/nunet/nunet-adapter:test
}

fake-news-score() {
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
    docker pull registry.gitlab.com/nunet/fake-news-detection/fake_news_score/fake_news_score_module:temp
    docker tag registry.gitlab.com/nunet/fake-news-detection/fake_news_score/fake_news_score_module:temp registry.gitlab.com/nunet/fake-news-detection/fake_news_score/fake_news_score_module:test
}

uclnlp() {
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
    docker pull registry.gitlab.com/nunet/fake-news-detection/uclnlp/uclnlp_module:temp
    docker tag uclnlp_module:latest registry.gitlab.com/nunet/fake-news-detection/uclnlp/uclnlp_module:temp registry.gitlab.com/nunet/fake-news-detection/uclnlp/uclnlp_module:test
}

athene() {
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
    docker pull registry.gitlab.com/nunet/fake-news-detection/athene/athene_snet_grpc:temp
    docker tag registry.gitlab.com/nunet/fake-news-detection/athene/athene_snet_grpc:temp registry.gitlab.com/nunet/fake-news-detection/athene/athene_snet_grpc:test


    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
    docker pull registry.gitlab.com/nunet/fake-news-detection/athene/athene_system:temp
    docker tag registry.gitlab.com/nunet/fake-news-detection/athene/athene_system:temp registry.gitlab.com/nunet/fake-news-detection/athene/athene_system:test


    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
    docker pull registry.gitlab.com/nunet/fake-news-detection/athene/stanfordnlp:temp  
    docker tag registry.gitlab.com/nunet/fake-news-detection/athene/athene_system:temp registry.gitlab.com/nunet/fake-news-detection/athene/stanfordnlp:test

}

tokenomics() {
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD registry.gitlab.com
    docker pull registry.gitlab.com/nunet/tokenomics-api-eth:temp
    docker tag registry.gitlab.com/nunet/tokenomics-api-eth:temp registry.gitlab.com/nunet/tokenomics-api-eth:test

}

all() {
    nunet-adapter
    athene
    uclnlp
    fake-news-score
    tokenomics
}

help () {
    echo "Usage: bash swap_image.sh OPTION "
    echo "  Options:"
    echo "    nunet-adapter "
    echo "    fake-news-score "    
    echo "    uclnlp "
    echo "    athene "
    echo "    tokenomics "
    echo "    all "

}

case $1 in
    nunet-adapter) nunet-adapter ;;
    fake-news-score) fake-news-score ;;
    uclnlp) uclnlp ;;
    athene) athene ;;
    tokenomics) tokenomics;;
    all) all ;;
  *) help ;;
esac
