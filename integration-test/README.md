# Test

## Clone repos

``` 
    bash clone.sh rest-api
    bash clone.sh nunet-adapter
    bash clone.sh stats-db
    bash clone.sh athene
    bash clone.sh uclnlp
    bash clone.sh fake-news-score
    bash clone.sh tokenomics
```

## Build docker images
``` 
    export user_name
    export password
    export db_name

    bash build.sh rest-api
    bash build.sh nunet-adapter
    bash build.sh stats-db
    bash build.sh athene
    bash build.sh uclnlp
    bash build.sh fake-news-score
    bash build.sh tokenomics

```

## Run services
``` 
    bash up.sh rest-api
    bash up.sh nunet-adapter-news-score
    bash up.sh nunet-adapter-uclnlp
    bash up.sh nunet-adapter-athene
    bash up.sh stats-db
    bash up.sh athene
    bash up.sh uclnlp
    bash up.sh fake-news-score
    bash up.sh tokenomics

```


## To setup all
### Since stats-db also used by nunet-demo it is not included on 'up.sh all'.

``` 
    bash clone.sh all
    bash build.sh all
    bash up.sh all

```


# To test

## build docker image of the test
``` 
    docker build --build-arg db_name=$db_name \
    --build-arg user_name=$user_name \
    --build-arg password=$password --build-arg rpc_address=$rpc_address -t integration_test .

```
## run test
``` 
    docker run -it integration_test:latest bash -c "pytest"
```