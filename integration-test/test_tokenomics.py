import sys
import grpc
from extract import *

import json

import os
from web3 import Web3

sys.path.append("./service_spec")
import nunet_adapter_pb2 as pb2
import nunet_adapter_pb2_grpc as pb2_grpc

import pytest
import time
import utils


@pytest.fixture
def input_value():
    url="https://bbc.com"
    headline = extract_headline(url)
    body = extract_body(url)
    return headline,body


@pytest.fixture
def web3_params():
    contract_address="0x74e5Adc587B38d221532A99BB0f85b8b4b50A87A"
    rpc_address= os.environ['rpc_address']
    web3=Web3(Web3.HTTPProvider(rpc_address))
    abi=json.load(open("abi.json", "r"))["abi"]
    contract=web3.eth.contract(address=Web3.toChecksumAddress(contract_address),abi=abi)

    return contract

def test_nunet_adapter(input_value,web3_params):
    contract=web3_params
    public_key_uclnlp="0xb5114121A51c6FfA04dBC73F26eDb7B6bfE2eB35"
    public_key_athene="0x17BA49f80a4Bcb8Ed3c66Ab107bBB40EC3a63370"
    public_key_news_score="0xB617FeB5178e92f2804068F772d924fC725705F2"
    
    headline = input_value[0]
    body = input_value[1]



    #before service call
    before_balance_uclnlp = contract.functions.balanceOf(public_key_uclnlp).call()
    before_balance_athene = contract.functions.balanceOf(public_key_athene).call()
    before_balance_news_score = contract.functions.balanceOf(public_key_news_score).call()

    req_address=utils.get_address("testing-nunet-adapter-news-score")
    address=req_address[0]
    port=req_address[1]
    serviceAddress=address+":"+port

    channel = grpc.insecure_channel("{}".format(serviceAddress))
    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.ServiceDefnition()
    fp.service_name = 'testing-news-score'
    fp.params = '{"body":"'+body+'","headline":"'+headline+'"}'
    result = stub.reqService(fp)


    #after service call
    after_balance_uclnlp = contract.functions.balanceOf(public_key_uclnlp).call()
    after_balance_athene = contract.functions.balanceOf(public_key_athene).call()
    after_balance_news_score = contract.functions.balanceOf(public_key_news_score).call()
    assert before_balance_uclnlp < after_balance_uclnlp
    assert before_balance_athene < after_balance_athene
    assert before_balance_news_score < after_balance_news_score
