job "testing-uclnlp" {
    datacenters = ["testing-nunet-io"]

    group "testing-uclnlp" {
        count = 1
        task "testing-uclnlp" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/fake-news-detection/uclnlp/uclnlp_module:test"
                args = ["python3", "run_uclnlp_service.py", "--daemon-config", "snetd.config.json", "--grpc-port", " ${NOMAD_PORT_rpc}"]
            }
        env  {
            nunet_adapter_name = "testing-nunet-adapter-uclnlp"
        }
            resources {
                cpu    = 2000 # MHz
                memory = 8560 # MB
                network {
                    port "rpc" {}
                }
            }

            service {
                name = "testing-uclnlp"
                port = "rpc"

                tags = [
                    "theNunetMachine",
                    "urlprefix-/testing-uclnlp proto=grpc",
                ]

                check {
                    type     = "script"
                    name     = "dummy"
                    command  = "/bin/echo"
                    args     = ["hello"]
                    interval = "60s"
                    timeout  = "30s"
                    check_restart {
                      limit = 3
                      grace = "90s"
                      ignore_warnings = false
                    }
                }
            }

        }
    }
}


