job "testing-news-score" {
    datacenters = ["testing-nunet-io"]

    group "testing-news-score" {
        count = 1
        task "testing-news-score" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/fake-news-detection/fake_news_score/fake_news_score_module:test"
                args = ["python3", "run_fake_news_score.py", "--daemon-config", "snetd.config.json", "--grpc-port", " ${NOMAD_PORT_rpc}"]
            }
        env  {
            nunet_adapter_name = "testing-nunet-adapter-news-score"
            nunet_adapter_uclnlp_name = "testing-nunet-adapter-uclnlp"
            nunet_adapter_athene_name = "testing-nunet-adapter-athene"
        }
            resources {
                cpu    = 2000 # MHz
                memory = 1000 # MB
                network {
                    port "rpc" {}
                }
            }

            service {
                name = "testing-news-score"
                port = "rpc"

                tags = [
                    "theNunetMachine",
                    "urlprefix-/testing-news-score proto=grpc",
                ]

                check {
                    type     = "script"
                    name     = "dummy"
                    command  = "/bin/echo"
                    args     = ["hello"]
                    interval = "60s"
                    timeout  = "30s"
                    check_restart {
                      limit = 3
                      grace = "90s"
                      ignore_warnings = false
                    }
                }
            }

        }
    }
}


