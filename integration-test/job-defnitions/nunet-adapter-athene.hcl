job "testing-nunet-adapter-athene" {
    datacenters = ["testing-nunet-io"]

    group "testing-nunet-adapter-athene" {
        count = 1
        task "testing-nunet-adapter-athene" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/nunet-adapter:test"
                args = ["python3", "nunet_adapter.py", " ${NOMAD_PORT_rpc}"]
            }
        env  {
            db_name = "nunet_db"
            user_name = "nunet"
            password = "nunet"
            device_name="athene"
            service_name="athene"
	        service_address="testing-athene"
            tokenomics_api_name="testing-tokenomics"
        }
            resources {
                cpu    = 2000 # MHz
                memory = 1000 # MB
                network {
                    port "rpc" {}
                }
            }

            service {
                name = "testing-nunet-adapter-athene"
                port = "rpc"

                tags = [
                    "theNunetMachine",
                    "urlprefix-/testing-nunet-adapter-athene proto=grpc",
                ]

                check {
                    type     = "script"
                    name     = "dummy"
                    command  = "/bin/echo"
                    args     = ["hello"]
                    interval = "60s"
                    timeout  = "30s"
                    check_restart {
                      limit = 3
                      grace = "90s"
                      ignore_warnings = false
                    }
                }
            }

        }
    }
}
