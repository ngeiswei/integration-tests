job "testing-athene-system" {
    datacenters = ["testing-nunet-io"]

    group "testing-athene-system" {
        count = 1
        task "testing-athene-system" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/fake-news-detection/athene/athene_system:test"
                args = ["/bin/bash", "/root/athene_system/fnc/run.sh", " ${NOMAD_PORT_rpc}"]
            }

            resources {
                cpu    = 2000 # MHz
                memory = 8560 # MB
                network {
                    port "rpc" {}
                }
            }

            service {
                name = "testing-athene-system"
                port = "rpc"

                tags = [
                    "theNunetMachine",
                    "urlprefix-/testing-athene-system proto=grpc",
                ]

                check {
                    type     = "script"
                    name     = "dummy"
                    command  = "/bin/echo"
                    args     = ["hello"]
                    interval = "60s"
                    timeout  = "30s"
                    check_restart {
                      limit = 3
                      grace = "90s"
                      ignore_warnings = false
                    }
                }
            }

        }
    }
}


