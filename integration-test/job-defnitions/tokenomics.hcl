job "testing-tokenomics" {
    datacenters = ["testing-nunet-io"]

    group "testing-tokenomics" {
        count = 1
        task "testing-tokenomics" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/tokenomics-api-eth:test"
                args = ["python3", "tokenomics.py", " ${NOMAD_PORT_rpc}"]
            }
        env  {
            CONFIG = "TEST"
            rpc_address="http://135.181.222.170:8545"
        }
            resources {
                cpu    = 2000 # MHz
                memory = 1000 # MB
                network {
                    port "rpc" {}
                }
            }

            service {
                name = "testing-tokenomics"
                port = "rpc"

                tags = [
                    "urlprefix-/testing-tokenomics proto=grpc",
                ]

                check {
                    type     = "script"
                    name     = "dummy"
                    command  = "/bin/echo"
                    args     = ["hello"]
                    interval = "60s"
                    timeout  = "30s"
                    check_restart {
                      limit = 3
                      grace = "90s"
                      ignore_warnings = false
                    }
                }
            }

        }
    }
}


