job "testing-nunet-adapter-news-score" {
    datacenters = ["testing-nunet-io"]

    group "testing-nunet-adapter-news-score" {
        count = 1
        task "testing-nunet-adapter-news-score" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/nunet-adapter:test"
                args = ["python3", "nunet_adapter.py", " ${NOMAD_PORT_rpc}"]
            }
        env  {
            db_name = "nunet_db"
            user_name = "nunet"
            password = "nunet"
            device_name="news_score"
            service_name="news-score"
	        service_address="testing-news-score"
            tokenomics_api_name="testing-tokenomics"
        }
            resources {
                cpu    = 2000 # MHz
                memory = 1000 # MB
                network {
                    port "rpc" {}
                }
            }

            service {
                name = "testing-nunet-adapter-news-score"
                port = "rpc"

                tags = [
                    "theNunetMachine",
                    "urlprefix-/testing-nunet-adapter-news-score proto=grpc",
                ]

                check {
                    type     = "script"
                    name     = "dummy"
                    command  = "/bin/echo"
                    args     = ["hello"]
                    interval = "60s"
                    timeout  = "30s"
                    check_restart {
                      limit = 3
                      grace = "90s"
                      ignore_warnings = false
                    }
                }
            }

        }
    }
}
