job "testing-athene-stanfordnlp" {
    datacenters = ["testing-nunet-io"]

    group "testing-athene-stanfordnlp" {
        count = 1
        task "testing-athene-stanfordnlp" {
            driver = "docker"

            config {
                image = "registry.gitlab.com/nunet/fake-news-detection/athene/stanfordnlp:test"
                args = ["java", "-mx4g", "-cp", "*", "edu.stanford.nlp.pipeline.StanfordCoreNLPServer", "-port", " ${NOMAD_PORT_rpc}"]
            }

            resources {
                cpu    = 2000 # MHz
                memory = 8560 # MB
                network {
                    port "rpc" {}
                }
            }

            service {
                name = "testing-athene-stanfordnlp"
                port = "rpc"

                tags = [
                    "theNunetMachine",
                    "urlprefix-/testing-athene-stanfordnlp proto=grpc",
                ]

                check {
                    type     = "script"
                    name     = "dummy"
                    command  = "/bin/echo"
                    args     = ["hello"]
                    interval = "60s"
                    timeout  = "30s"
                    check_restart {
                      limit = 3
                      grace = "90s"
                      ignore_warnings = false
                    }
                }
            }

        }
    }
}


