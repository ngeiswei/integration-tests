import requests
import pytest

def test_rest_api():
    res=requests.get(url="http://testserver.nunet.io:7006/get_score", params= {"url":"https://bbc.com"})
    assert "related" in str(res) or "agree" in str(res)