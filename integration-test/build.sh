#!/bin/bash


nunet-adapter() {
	docker build -t nunet_adapter /home/$USER/nunet-adapter/
}

rest-api() {
    docker-compose -f /home/$USER/rest-api/rest-api/docker-compose.yml build
}

stats-db() {
    docker-compose -f /home/$USER/stats-database/database/docker-compose.yml build
}

fake-news-score() {
    docker build -t snet_publish_service https://github.com/singnet/dev-portal.git#master:/tutorials/docker
	docker build -t fns_snet /home/$USER/fake_news_score/
}

uclnlp() {
    docker build -t snet_publish_service https://github.com/singnet/dev-portal.git#master:/tutorials/docker
	docker build -t uclnlp_snet /home/$USER/uclnlp/
}

athene() {
    docker build -t snet_publish_service https://github.com/singnet/dev-portal.git#master:/tutorials/docker
    docker-compose -f /home/$USER/athene/docker-compose.yml build
}

tokenomics() {
	docker build --build-arg CONFIG="TEST" --build-arg rpc_address=$rpc_address -t  tokenomics  /home/$USER/tokenomics-api-eth/
}

all() {
    rest-api
    nunet-adapter


    stats-db
    athene
    uclnlp
    fake-news-score
    tokenomics
}

help () {
    echo "Usage: bash build.sh OPTION [SERVICE]"
    echo "  Options:"
    echo "    rest-api        build the rest-api docker image"
    echo "    nunet-adapter        build the nunet-adapter docker image"

    echo "    stats-db        build the stats-db docker image"
    echo "    fake-news-score        build the fake-news-score docker image"    
    echo "    uclnlp        build the uclnlp docker image"
    echo "    athene        build the athene docker image"
    echo "    all        build all docker image"

}

case $1 in
    rest-api) rest-api ;;
    nunet-adapter) nunet-adapter ;;

    stats-db) stats-db ;;
    fake-news-score) fake-news-score ;;
    uclnlp) uclnlp ;;
    athene) athene ;;
    tokenomics) tokenomics;;
    all) all ;;
  *) help ;;
esac
