#!/bin/bash

nunet-adapter-news-score() {
    nomad job stop testing-nunet-adapter-news-score
    nomad job run integration-test/job-defnitions/nunet-adapter-news-score.hcl
}

nunet-adapter-uclnlp() {
    nomad job stop testing-nunet-adapter-uclnlp
    nomad job run integration-test/job-defnitions/nunet-adapter-uclnlp.hcl
}

nunet-adapter-athene() {
    nomad job stop testing-nunet-adapter-athene
    nomad job run integration-test/job-defnitions/nunet-adapter-athene.hcl
}

fake-news-score() {
    nomad job stop testing-news-score
    nomad job run integration-test/job-defnitions/fake-news-score.hcl
}

uclnlp() {
    nomad job stop testing-uclnlp
    nomad job run integration-test/job-defnitions/uclnlp.hcl
}

athene() {
    nomad job stop testing-athene-system
    nomad job run integration-test/job-defnitions/athene-system.hcl

    nomad job stop testing-athene-stanfordnlp
    nomad job run integration-test/job-defnitions/athene-stanfordnlp.hcl

    nomad job stop testing-athene
    nomad job run integration-test/job-defnitions/athene-snet-grpc.hcl
}

tokenomics() {
    nomad job stop testing-tokenomics
    nomad job run integration-test/job-defnitions/tokenomics.hcl
}

all() {
    nunet-adapter-news-score
    nunet-adapter-uclnlp
    nunet-adapter-athene
    athene
    uclnlp
    fake-news-score
    tokenomics
}


help () {
    echo "Usage: bash up.sh OPTION [SERVICE]"
    echo "  Options:"
    echo "    rest-api        up the rest-api "
    
    echo "    nunet-adapter-news-score        up the nunet-adapter-news-score "
    echo "    nunet-adapter-uclnlp        up the nunet-adapter-uclnlp "
    echo "    nunet-adapter-athene        up the nunet-adapter-athene "

    echo "    stats-db        up the stats-db "
    echo "    fake-news-score        up the fake-news-score "    
    echo "    uclnlp        up the uclnlp "
    echo "    athene        up the athene "
    echo "    tokenomics        up the tokenomics"

    echo "    all        up all "

}

case $1 in
    rest-api) rest-api ;;
    nunet-adapter-news-score) nunet-adapter-news-score ;;
    nunet-adapter-uclnlp) nunet-adapter-uclnlp ;;
    nunet-adapter-athene) nunet-adapter-athene ;;
    stats-db) stats-db ;;
    fake-news-score) fake-news-score ;;
    uclnlp) uclnlp ;;
    athene) athene ;;
    tokenomics) tokenomics ;;
    all) all;;
  *) help ;;
esac
