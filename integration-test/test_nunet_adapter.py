import sys
import grpc
from extract import *

sys.path.append("./service_spec")
import nunet_adapter_pb2 as pb2
import nunet_adapter_pb2_grpc as pb2_grpc

from db import ProviderDevice
from db.interface import Database

import os

import pytest
import utils

@pytest.fixture
def input_value():
    url="https://bbc.com"
    headline = extract_headline(url)
    body = extract_body(url)
    return headline,body

@pytest.fixture
def db_params():
    password = os.environ['password']
    user_name = os.environ['user_name']   
    db_name = os.environ['db_name']

    host="demo.nunet.io"
    port="5431"
    return user_name,password,db_name,host,port

def test_nunet_adapter(input_value,db_params):
    #db = Database(user_name,password, db_name, host, port)
    db = Database(db_params[0],db_params[1], db_params[2], db_params[3], db_params[4])
    req_address=utils.get_address("testing-nunet-adapter-news-score")
    address=req_address[0]
    port=req_address[1]
    serviceAddress=address+":"+port
    channel = grpc.insecure_channel("{}".format(serviceAddress))
    headline = input_value[0]
    body = input_value[1]

    #before service call
    pre_call_uclnlp=db.query(ProviderDevice,device_name="uclnlp")
    pre_call_athene=db.query(ProviderDevice,device_name="athene")
    pre_call_news_score=db.query(ProviderDevice,device_name="news_score")
    
    assert pre_call_uclnlp.device_name == "uclnlp"
    assert pre_call_athene.device_name == "athene"
    assert pre_call_news_score.device_name == "news_score"

    stub = pb2_grpc.NunetAdapterStub(channel)
    fp = pb2.ServiceDefnition()
    fp.service_name = 'testing-news-score'
    fp.params = '{"body":"'+body+'","headline":"'+headline+'"}'
    result = stub.reqService(fp)

    #after service call

    db = Database(db_params[0],db_params[1], db_params[2], db_params[3], db_params[4])

    after_call_uclnlp=db.query(ProviderDevice,device_name="uclnlp")
    after_call_athene=db.query(ProviderDevice,device_name="athene")
    after_call_news_score=db.query(ProviderDevice,device_name="news_score")

    assert pre_call_uclnlp.process_completed +1 == after_call_uclnlp.process_completed
    assert pre_call_athene.process_completed +1 == after_call_athene.process_completed
    assert pre_call_news_score.process_completed  +1 == after_call_news_score.process_completed

    assert "related" in str(result) or "agree" in str(result)
