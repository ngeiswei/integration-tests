import sys
import grpc
from extract import *

sys.path.append("./service_spec")
import fake_news_score_pb2 as pb2
import fake_news_score_pb2_grpc as pb2_grpc

import pytest
import utils

@pytest.fixture
def input_value():
    url="https://bbc.com"
    headline = extract_headline(url)
    body = extract_body(url)
    return headline,body

def test_fakenews_score(input_value):
    req_address=utils.get_address("testing-news-score")
    address=req_address[0]
    port=req_address[1]
    serviceAddress=address+":"+port
    channel = grpc.insecure_channel("{}".format(serviceAddress))
    stub = pb2_grpc.FakeNewsScoreStub(channel)
    example = pb2.InputFNS()
    example.headline = input_value[0]
    example.body = input_value[1]
    res = stub.fn_score_calc(example)
    assert "related" in str(res) or "agree" in str(res) or "discuss" in str(res)
