#!/bin/bash

cd /home/$USER/

nunet-adapter() {
    if [ -d "/home/$USER/nunet-adapter" ] 
    then
        echo "Directory /home/$USER/nunet-adapter exists." 
    else
        git clone https://gitlab.com/nunet/nunet-adapter.git
    fi
}

rest-api() {
    if [ -d "/home/$USER/rest-api" ] 
    then
        echo "Directory /home/$USER/rest-api exists." 
    else
        git clone https://gitlab.com/nunet/rest-api.git
    fi
}

stats-db() {
    if [ -d "/home/$USER/stats-database" ] 
    then
        echo "Directory /home/$USER/stats-database exists." 
    else
        git clone https://gitlab.com/nunet/stats-database.git
    fi
}

fake-news-score() {
    if [ -d "/home/$USER/fake_news_score" ] 
    then
        echo "Directory /home/$USER/fake_news_score exists." 
    else
        git clone https://gitlab.com/nunet/fake-news-detection/fake_news_score.git
    fi
}

uclnlp() {
    if [ -d "/home/$USER/uclnlp" ] 
    then
        echo "Directory /home/$USER/uclnlp exists." 
    else
        git clone https://gitlab.com/nunet/fake-news-detection/uclnlp.git
    fi
}

athene() {
    if [ -d "/home/$USER/athene" ] 
    then
        echo "Directory /home/$USER/athene exists." 
    else
        git clone https://gitlab.com/nunet/fake-news-detection/athene.git
    fi
}

tokenomics() {
    if [ -d "/home/$USER/tokenomics-api-eth" ] 
    then
        echo "Directory /home/$USER/tokenomics-api-eth exists." 
    else
        git clone https://gitlab.com/nunet/tokenomics-api-eth.git
    fi
}


all() {
    rest-api
    nunet-adapter
    stats-db
    athene
    uclnlp
    fake-news-score
    tokenomics
}

help () {
    echo "Usage: bash clone.sh OPTION [SERVICE]"
    echo "  Options:"
    echo "    rest-api        clone rest-api "
    echo "    nunet-adapter        clone nunet-adapter "
    echo "    stats-db        clone stats-db "
    echo "    fake-news-score        clone fake-news-score "    
    echo "    uclnlp        clone uclnlp "
    echo "    athene        clone athene "
    echo "    tokenomics        clone tokenomics "
    echo "    all        clone all "

}

case $1 in
    rest-api) rest-api ;;
    nunet-adapter) nunet-adapter ;;
    stats-db) stats-db ;;
    fake-news-score) fake-news-score ;;
    uclnlp) uclnlp ;;
    athene) athene ;;
    all) all;;
    tokenomics) tokenomics ;;
  *) help ;;
esac
