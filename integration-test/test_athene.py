import sys
import grpc
from extract import *

sys.path.append("./service_spec")
import athenefnc_pb2 as pb2
import athenefnc_pb2_grpc as pb2_grpc
import pytest
import utils

@pytest.fixture
def input_value():
    url="https://bbc.com"
    headline = extract_headline(url)
    body = extract_body(url)
    return headline,body

def test_athene(input_value):
    req_address=utils.get_address("testing-athene")
    address=req_address[0]
    port=req_address[1]
    serviceAddress=address+":"+port
    channel = grpc.insecure_channel("{}".format(serviceAddress))
    stub = pb2_grpc.AtheneStanceClassificationStub(channel)
    in_d = pb2.InputData()
    in_d.headline = input_value[0]
    in_d.body = input_value[1]
    res = stub.stance_classify(in_d)
    assert "related" in str(res) or "agree" in str(res)