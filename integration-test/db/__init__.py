from sqlalchemy import Column, Integer, String, ForeignKey ,Float ,DateTime , Boolean , Binary
from sqlalchemy.orm import relationship
import datetime
from .interface import Base

class Execution(Base):
    __tablename__ = "execution"
    execution_id = Column(Integer, primary_key=True)
    subprocess_id = Column(Integer,default= 0)
    input_image = Column(String(), default="")
    image_output = Column(String(), default="")
    date = Column(DateTime, default=datetime.datetime.now())
    cpu_usage = Column(Float(precision=4), default=0.0)
    time_taken = Column(Float(precision=4), default=0.0)
    memory_usage = Column(Float(precision=4), default = 0.0)
    time_of_run = Column(Float(precision=4), default = 0.0)
    token_earned = Column(Integer, default = 0)
    result = Column(String(), default="")
    network_tx = Column(Float(precision=4), default =0.0)
    network_rx = Column(Float(precision=4), default = 0.0)
    dev_id = Column(Integer)
    json_result = Column(String(), default="")
    total_memory = Column(Float(), default=0.0)

class ProviderDevice(Base):
    __tablename__ = "providerdevice"
    dev_id = Column(Integer, primary_key=True)
    device_name = Column(String(256),
                         nullable=False)
    process_completed = Column(Integer , default= 0 )
    token_earned = Column(Float, default = 0.0)
    memory_limit = Column(Float, default = 0.0)
    memory_used = Column(Float, default = 0.0)
    net_limit = Column(Float, default = 0.0)
    net_used = Column(Float, default = 0.0)
    cpu_limit = Column(Float, default = 0.0)
    cpu_used = Column(Float, default = 0.0)
    up_time_limit = Column(Float, default  = 0.0)
    up_time_used = Column(Float, default = 0.0)
    cpu_price = Column(Float, default = 0.0)
    ram_price = Column(Float , default =0.0)
    net_price = Column(Float, default = 0.0)
    device_public_key = Column(String(256),
                         nullable=True)



    def __repr__(self):
        return  "DEVICE_NAME : {}\n" \
                "PROCESS_COMPLETED : {}\n" \
               "TOKEN_EARNED: {}\n".format(self.process_completed,
                                           self.device_name,
                                           self.token_earned)
