import requests
import json 
def get_address(service_name):   
    service_description=requests.get("https://consul.icog-labs.com/v1/catalog/service/"+service_name)
    service_description=service_description.content
    service_description=json.loads(service_description.decode('utf8'))[0]
    service_address=service_description["ServiceAddress"]
    service_port=service_description["ServiceTaggedAddresses"]["lan_ipv4"]["Port"]
    return str(service_address),str(service_port)
